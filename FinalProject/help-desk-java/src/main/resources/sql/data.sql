INSERT INTO user VALUES (null,'Firstname1','Lastname1', 1, 'user1_mogilev@yopmail.com', '$2y$10$.yrFSN2XM635z5.qc63QueptoKdq5vNC2NWqjCpB2gHUSGsM16OYq');
INSERT INTO user VALUES (null,'Firstname2','Lastname2', 1, 'user2_mogilev@yopmail.com', '$2y$10$.yrFSN2XM635z5.qc63QueptoKdq5vNC2NWqjCpB2gHUSGsM16OYq');
INSERT INTO user VALUES (null,'Firstname3','Lastname3', 2, 'manager1_mogilev@yopmail.com', '$2y$10$.yrFSN2XM635z5.qc63QueptoKdq5vNC2NWqjCpB2gHUSGsM16OYq');
INSERT INTO user VALUES (null,'Firstname4','Lastname4', 2, 'manager2_mogilev@yopmail.com', '$2y$10$.yrFSN2XM635z5.qc63QueptoKdq5vNC2NWqjCpB2gHUSGsM16OYq');
INSERT INTO user VALUES (null,'Firstname5','Lastname5', 3, 'engineer1_mogilev@yopmail.com', '$2y$10$.yrFSN2XM635z5.qc63QueptoKdq5vNC2NWqjCpB2gHUSGsM16OYq');
INSERT INTO user VALUES (null,'Firstname6','Lastname6', 3, 'engineer2_mogilev@yopmail.com', '$2y$10$.yrFSN2XM635z5.qc63QueptoKdq5vNC2NWqjCpB2gHUSGsM16OYq');

INSERT INTO ticket VALUES (null, 'task1', 'Description1', '2020-02-01', '2020-02-01', 5, 1, 5, 1, 3, 3, null);
INSERT INTO ticket VALUES (null, 'task2', 'Description2', '2020-02-02', '2020-03-02', 5, 1, 3, 1, 3, 3, null);
INSERT INTO ticket VALUES (null, 'task3', 'Description3', '2020-02-03', '2020-03-03', 6, 2, 3, 1, 4, 3, null);
INSERT INTO ticket VALUES (null, 'task4', 'Description4', '2020-02-04', '2020-03-04', 5, 3, 6, 1, 2, 4, null);

INSERT INTO category VALUES (null, 'Application & Services');
INSERT INTO category VALUES (null, 'Benefits & Paper Work');
INSERT INTO category VALUES (null, 'Hardware & Software');
INSERT INTO category VALUES (null, 'People Management');
INSERT INTO category VALUES (null, 'Security & Access');
INSERT INTO category VALUES (null, 'Workplaces & Facilities');
