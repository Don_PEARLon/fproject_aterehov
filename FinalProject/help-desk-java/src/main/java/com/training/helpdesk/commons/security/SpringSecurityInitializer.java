package com.training.helpdesk.commons.security;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;
import org.springframework.stereotype.Component;

@Component
public class SpringSecurityInitializer extends AbstractSecurityWebApplicationInitializer {

}