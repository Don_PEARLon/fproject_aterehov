The improved version of the "Help Desk" application.
Logic of performing ticket state actions has been changed.

To run Java back-end application go in cmd to the "help-desk-java" folder and print command:
"mvn tomcat7:run" or "mvn clean install tomcat7:run-war-only".
Swagger documentation will be available by URL "http://localhost:8080/help-desk".

To run ReactJs front-end application go in cmd to the "help-desk-react" folder and print command:
"npm start". Please, make sure that all necessary components are already installed.
You need to install:
npm install --save react-router react-router-dom axios
npm i react-table-6 --save.
React application will be available by URL "http://localhost:3000/help-desk"

Existing users and their roles:

Role 'Employee':
user1_mogilev@yopmail.com/P@ssword1
user2_mogilev@yopmail.com/P@ssword1 

Role 'Manager':
manager1_mogilev@yopmail.com/P@ssword1
manager2_mogilev@yopmail.com/P@ssword1

Role 'Engineer':
engineer1_mogilev@yopmail.com/P@ssword1
engineer2_mogilev@yopmail.com/P@ssword1

